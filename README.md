# FEELTREES

Zde jsou nástroje pro vývojáře komunitní vědomé sociální sítě se záměrem :-)

## struktura projektu

- originální projekty feeltrees jsou zde jako git submoduly v adresářích _./ft2_ a _./ft3_ a jsou na tomto projektu nezávislé
- nástroje jsou vytvářeny v jazyce [nim](https://nimlang.org). Používám vlastní vývojářské nástroje - balíček _pruga_, který je v neustálém divokém vývoji. Snažím se o nezávislost na tomto balíčku, má vysoce nestabilní api, neposkytuji žádnou podporu atd. Může se stát, že se někde nějaká závislost objeví, pak napiš a dám ti chybějící zdrojáky.

## úprava šablon

@TODO: popsat jaké úpravy dělá

```shell

twig2vue ./ft2/templates/home/page.html.twig

```

všechny šablony převede tento příkaz (využijeme wildcards shellu)

```shell
# funguje v shellu fish
twig2vue ./ft2/templates/*.html.twig ./ft2/templates/**/*.html.twig

```

## html validator

Příkaz ./bin/htmlValidator vypíše chyby při parsování html souboru - používáme na twig šablony, které by měly být html validní (ale nemusí).
