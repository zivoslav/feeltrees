#!/usr/bin/env fish

clear

echo "Пруга: dev"

# spouštěj příkazy

# nim build
pruga-compile htmlValidator --force #--  --output=./ft2/twig2vue/html.validator ./ft2/templates/*.html.twig ./ft2/templates/**/*.html.twig
pruga-compile twig2vue --force -- ft2/templates/security/login.html.twig # ./ft2/templates/home/page.html.twig

cd ft2
../bin/twig2vue --output=twig2vue ./templates/*.html.twig ./templates/**/*.html.twig

../bin/twig2vue --output=twig2vue ./assets/js/components/*.vue ./assets/js/components/**/*.vue

# pruga-test ./src/twig2vue/html.nim
# pruga-test ./src/twig2vue/ahref2route.nim
#pruga make
#pruga-compile --force

# vývoj
#php ./dev.php $1
# python ./dev.py $1
#nim compile ./dev.nim $1
#pruga-compile ./dev.nim -- --file $1


