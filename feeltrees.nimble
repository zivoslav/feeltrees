
# Package

version       = "0.1.0"
author        = "feeltrees"
description   = "The utils for feeltrees developers"
license       = "MIT"
srcDir        = "src"



# Dependencies

requires "nim >= 1.5.1"
requires "pruga"

