
#!/usr/bin/env nim
mode = ScriptMode.Verbose
# mode = ScriptMode.Silent

switch("app", "console")
switch("colors", "on")

switch("define", "PROJECT_DIR:" & thisDir())


task build, "build ":
    
    exec "pruga-compile --force"

