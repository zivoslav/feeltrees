
import docopt
import pkg/twig2vue/ahref2route
import pkg/twig2vue/fontAwesome
import pkg/twig2vue/writer
import pruga/cli/terminal
import std/htmlparser
import std/os
import std/streams
import std/strutils
import std/strutils
import system
import std/terminal as stdTerminal
import std/xmltree

const DEFAULT_DIR* = "./twig2vue-output"

proc parseFile(file: string): (XmlNode, seq[string]) =
    ## parsuje html soubor (šablonu twig)
    ## vrátí xmltree už upravenou
    echo "=".repeat 64
    styledEcho fgBlue, "parsuji soubor: ", fgGreen, file
    echo "=".repeat 64
    echo ""

    
    let stream = file.newFileStream()
    let doc = stream.readAll().replace(" @", " v-on:")
    styledEcho bgRed, doc
    let source = doc.newStringStream()

    var errors: seq[string] = @[]
    let document = source.parsehtml(file, errors)

    if errors.len() > 0:
        styledEcho fgYellow, "html není validní v ", fgBlue, file
        for error in errors:
            styledEcho bgRed, fgBlack, error

    # styledEcho bgGreen, fgWhite, $document

    return (document, errors)

proc doAction*(args: Table[string, Value]) =

    # echo args
    
    let sources = args["<sources>"]
    let output = if args["--output"]: $args["--output"] else: DEFAULT_DIR

    discard output.mkdirs()
        
    if not output.existsOrCreateDir():
        styledEcho fgRed, "nejestvuje výstupní adresář", fgBlue, " vytvořil jsem ", fgYellow, output

    

    for file in sources.items():

        let (document, errors) = file.parseFile()

        document.search_font_awesome()

        document.a_href2route_to()

        let outputFile = output / file

        discard mkdirs outputFile.parentDir()

        document.writeTo outputFile