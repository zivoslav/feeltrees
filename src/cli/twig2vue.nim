import pkg/twig2vue
import pruga/cli
# import docopt

const DOC = """
$program

Usage:
    $program [--output=<dir>] <sources>...
    $program (-h | --help)
    $program --version

Options:
    --output=<dir>    výstupní adresář, když nejestvuje, vytvoří DEFAULT_DIR
    -h --help   Show this screen.
    --version   Show version.
"""

when isMainModule:

    DOC.docopt().doAction()