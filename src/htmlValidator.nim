
import docopt
import pkg/twig2vue/html
import pkg/twig2vue/writer
import std/htmlparser
import std/os
import std/terminal
import std/xmltree
import std/strutils
import system

const DEFAULT_OUTPUT = "./html.validator"
const BR = "=".repeat 64

proc parseFile(file: string): (XmlNode, seq[string]) =
    ## parsuje html soubor (šablonu twig)
    ## vrátí xmltree už upravenou
    echo "=".repeat 64
    styledEcho fgBlue, "parsuji soubor: ", fgGreen, file
    echo "=".repeat 64
    echo ""

    var errors: seq[string] = @[]
    let document = file.loadHtml errors

    if errors.len() > 0:
        styledEcho fgYellow, "html není validní v ", fgBlue, file
        for error in errors:
            styledEcho bgRed, fgBlack, error

    # styledEcho bgGreen, fgWhite, $document

    return (document, errors)


proc doAction*(args: Table[string, Value]) =

    # echo args
    
    let sources = args["<sources>"]
    let output = if args["--output"]: $args["--output"] else: DEFAULT_OUTPUT
        
    # if not output.existsFile():
    #     styledEcho fgRed, "nejestvuje výstupní adresář", fgBlue, " vytvořil jsem ", fgYellow, output

    

    let stream = newFileStream(output, fmWrite)
        # stdout = stream
        # stream.write readAll(stdin)
    for file in sources.items():

        let (document, errors) = file.parseFile()


        if errors.len == 0:
            
            stream.writeLine BR
            stream.write file
            stream.writeLine " - žádné chyby"
            stream.writeLine BR

        else:
            stream.writeLine BR
            stream.writeLine file
            stream.writeLine BR
            stream.writeLine ""
            for error in errors:
                stream.writeLine error

        stream.writeLine ""
        stream.writeLine ""

    stream.flush()
    stream.close() 

    # for file in sources.items():

    #     let (document, errors) = file.parseFile()

        
            
            
    #         # for error in errors:
    #         #     styledEcho fgRed, error

    #     else:

    #     # document.search_font_awesome()

    #         let outputFile = output / file

    #         document.writeTo outputFile