
import std/xmltree
import std/strtabs
import std/terminal
import std/strutils
import std/options
import pruga/html as prugaHtml  # kvůli unescape html &quot; ...

const CLASS = "class"
const FAS = "fas"
const FA = "fa"
const FA_prefix = "fa-"
const FONT_AWESOME_ICON = "font-awesome-icon"
const ICON = "icon"

type ATTR_REPLACE = tuple[icon: string, class: seq[string]]


proc filter_class_font_awesome(class_list: seq[string]):Option[ATTR_REPLACE] =
    ## filtruje třídy, které chceme/nechceme použít dále
    ## vrací dvojici - (název ikony, zbylé třídy)


    if not(FAS in class_list) and not(FA in class_list):
        return none(ATTR_REPLACE)

    # styledEcho fgRed, "má třídu ", fgBlue, FAS 
               
    var new_class_list: seq[string] = @[]
    var icon_name = ""

    for class_item in class_list:
        if class_item.startsWith FA_prefix:
            icon_name = class_item[FA_prefix.len()..^1]
        elif class_item == FAS:
            discard
        elif class_item == FA:
            discard
        else:
            new_class_list.add class_item

    if icon_name == "":
        # raise newException(Exception, "Chybí název fas-awesome ikony")
        styledEcho bgRed, fgWhite, "Chybí název fas-awesome ikony"
        icon_name = "UNKNOWN_ICON_NAME"

    return some((icon_name, new_class_list))

proc check_class_font_awesome(class: string):Option[ATTR_REPLACE] = 
    ## nakonec zbytečná funkce, která jen řetězec z class převede na pole
    let list = class.split(" ")

    return list.filter_class_font_awesome()


proc edit_font_awesome(element: XmlNode) =

    if element.kind == xnElement:

        let attrs = element.attrs()

        if attrs == nil:
            styledEcho fgBlue, "přeskočím: ", fgGreen, element.tag, fgBlue, styleItalic, " nemá žádné atributy"
            return

        styledEcho fgWhite, "<", fgMagenta, $element.tag, " ", fgBlue, $attrs, fgWhite, ">"

        if attrs.hasKey CLASS:
            let class = attrs[CLASS]
            styledEcho fgBlue, class

            let checked = class.check_class_font_awesome()
            if checked.isSome():
                let (icon_name, new_class) = checked.get()

                element.tag = FONT_AWESOME_ICON
                element.attrs()[CLASS] = new_class.join(" ")
                element.attrs()[ICON] = icon_name

proc search_font_awesome*(element: XmlNode) =
    ## rekurzivně projdeme všechny elementy#[  ]#
    ## a testujeme přítomnost attributu class
    if element.kind == xnElement:
        # echo "TAG", element.tag
        # if element.attrs != nil:
        #     styledEcho fgYellow, "ATTR"
        #     for atname, atvalue in element.attrs().pairs():
        #         echo atname
        # else:
        #     echo $element
        
        if element.attr(CLASS) != "":
            element.edit_font_awesome()

        for child in element:
            child.search_font_awesome()

when isMainModule:
    # test
    import std/htmlparser
    import std/xmltree

    const html = """
<div v-on:click="replyto(a.id) @click.prevent="replyto(a.id)">
    <i class="fa fa-user-secret mr-1 small"></i>
    <p class="weekday small m-0">{{ p.date_from|date('D')|trans }}</p>
</div>
"""

    let document = html.parseHtml()

    document.search_font_awesome()

    styledEcho bgRed, "zabalí to do <document>, což mě trochu sere"
    echo document
    styledEcho fgYellow, "takže"
    var s = ""
    for child in document:
        s.add child

     
    echo prugaHtml.unescape($document)
