
import std/xmltree
import std/strtabs
import std/terminal
import std/strutils
# import std/options
import pruga/html as prugaHtml  # kvůli unescape html &quot; ...

const HREF = "href"

proc a_href2route_to*(document: XmlNode) =

    for elem in document.findAll "a":
        let attrs = elem.attrs()
        
        # projdu atributy a vyskocim beze změn, když tam je nějaký v-on:
        for attr in attrs.keys:
            # echo attr
            if attr.startsWith "v-on":
                return

        if attrs.hasKey HREF:
            attrs["to"] = attrs[HREF]
            attrs.del HREF
        # echo elem
        elem.tag = "router-link"

when isMainModule:
    # test
    import std/htmlparser
    import std/xmltree

    const html = """
<div v-on:click="replyto(a.id) @click.prevent="replyto(a.id)">
    <a href="normalni/odkaz"></a>
    <a href="odkaz/s/udalosti" v-on:mousedown="pustimeMysicku()"></a>
</div>
"""

    let document = html.parseHtml()

    document.a_href2route_to()

    doAssert document.child("div").child("a").attr(HREF) == "odkaz/s/udalosti"
    doAssert document.child("div").child("router-link").attr("to") == "normalni/odkaz"
    doAssert document.child("div").child("router-link").attrs.hasKey(HREF) == false