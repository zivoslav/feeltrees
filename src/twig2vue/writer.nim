


import pruga/path
import std/xmltree
import std/streams
export streams
import pruga/html as prugaHtml  # kvůli unescape html &quot; ...

proc document2string(node: XmlNode): string =
    ## eliminuje ten obalovací tag doc

    result = ""

    if node.kind == xnElement and node.tag == "document":
        for el in node:
            result.add el
    else:
        result.add node

    result = prugaHtml.unescape result

proc writeTo*(document: XmlNode, path: Path) =

    echo document.document2string()

    path.writeFile document.document2string()

proc writeTo*(document: XmlNode, stream: Stream) =

    stream.write document.document2string()