#!/usr/bin/env fish

cd ft2

php bin/console doctrine:migrations:migrate

gnome-terminal --tab --title 'Symfony server' -- symfony server:start

cd ../ft3
gnome-terminal --tab --title 'yarn encore' -- yarn encore dev --watch

# xterm -e "symfony server:start" &
# xterm -e "yarn encore dev --watch" &
