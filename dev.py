#!/usr/bin/env python3

from pathlib import Path

from lxml.html import etree


class Feature:
    def __init__(self, file: Path):
        if not file.is_file():
            raise FileNotFoundError(f'Zdrojový soubor "{file}" nejestvuje.')

        self.__doc = etree.parse(str(file))

    @property
    def title(self):
        return self.__doc.find('title').text

    @property
    def icon(self):
        return self.__doc.find('image/icon').text

    @property
    def content(self):
        return self.__doc.find('content').text


def load(source=None):

    if source is None:
        source = Path(__file__).parent / 'features'

    source = Path(source)

    for html_file in source.glob('*.html'):

        yield Feature(html_file)


if __name__ == "__main__":

    html_file = Path("ft2/templates/security/login.html.twig")

    Feature(html_file)
